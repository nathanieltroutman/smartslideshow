#!/usr/bin/python
'''
Smart slid-show is a slide show that handles displaying portrait pictures on a landscape screen with intelligence. It
utilizes the following techniques to make the display look better than block-boxes.

1) Face-aware cropping

This finds faces in the picture and then attempts to crop the image down to landscape while keeping the faces
visible and centered.

2) Contextual Background

This creates a background to put behind the portrait picture based on the contents of the picture. There are 
several different techniques for generating the background.
 - Smearing
 - Zoom-Blur
  

@author: Nathaniel Troutman <nathanieltroutman@gmail.com>
'''

from PySide import QtCore, QtGui
import numpy as np
import cv2
import logging
import itertools
import time
from os import path
import sys
from Queue import LifoQueue
from subprocess import call, check_output
from __builtin__ import True
from random import shuffle
import signal

def init_logging():
    global logger
    logger = logging.getLogger('smart-slideshow')
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
init_logging()


class ImageViewer(QtGui.QMainWindow):
    def __init__(self, app, fullscreen=False, time_delay=None, use_on_off_schedule=False, use_face_detection=False, **kwargs):
        super(ImageViewer, self).__init__()

        self.app = app
        self.imageLabel = QtGui.QLabel()
        self.imageLabel.setBackgroundRole(QtGui.QPalette.Base)
        self.imageLabel.setSizePolicy(QtGui.QSizePolicy.Ignored,
                QtGui.QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)
        
        self.setCentralWidget(self.imageLabel)	

        self.setWindowTitle("Image Viewer")
        if fullscreen:
            self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
            self.window_size = app.desktop().width()
            r = float(app.desktop().height()) / app.desktop().width()  # 1050.0 / 1680.0  # 1080.0 / 1920
        else:
            self.window_size = 800
            r = 1050.0 / 1680.0

        self.resize(self.window_size, int(self.window_size * r))
        self.move((app.desktop().width() / 2) - (self.width() / 2), (app.desktop().height() / 2) - (self.height() / 2))

        logger.info("win-size: %s,%s", self.height(), self.width())
        self.__init_open_cv__()
        
        self.use_on_off_schedule = use_on_off_schedule
        logger.info('Using On-Off Schedule: %s', self.use_on_off_schedule)
        
        if fullscreen:
            self.showFullScreen()
        else:
            self.show()
        
        self.screen_on = False
        self.turn_screen_on()
        self.activateWindow()
        self.raise_()
        
        self.use_face_detection = use_face_detection
        
        self.time_delay = time_delay
        logger.info('Using Delay: %s', self.time_delay)
        self.image_queue = LifoQueue(maxsize=2)
    
    def __init_open_cv__(self):
        self.face_cascades = []
        xml_files = ['haarcascades/haarcascade_frontalface_default.xml'
            # cv2.CascadeClassifier('/usr/local/Cellar/opencv/2.4.12/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml'),
            # cv2.CascadeClassifier('/usr/local/Cellar/opencv/2.4.12/share/OpenCV/haarcascades/haarcascade_frontalface_alt2.xml')
            ]
        
        for xml_file in xml_files:
            if not xml_file.startswith("/"):
                xml_file = path.join(sys.path[0], xml_file)
            if not path.isfile(xml_file):
                logger.warn("Cannot find cascade file: %s", xml_file)
                continue
            
            self.face_cascades.append(cv2.CascadeClassifier(xml_file))
    
    def turn_screen_off(self):
        if not self.screen_on:
            return
        
        try:
            if path.isfile('/usr/bin/tvservice'):
                call(['/usr/bin/tvservice', '--off'])
            else:
                logger.info("No HDMI via TvService...Pretending to Turn On")
            self.screen_on = False
        except:
            pass
        
    def turn_screen_on(self):
        if self.screen_on:
            return
        
        try:
            if path.isfile('/usr/bin/tvservice'):
                if 'TV is off' in check_output(['/usr/bin/tvservice', '-s']):
                    call(['/usr/bin/tvservice', '-p'])
                call(['/bin/fbset', '-depth', '8'])
                call(['/bin/fbset', '-depth', '16'])
            else:
                logger.info("No HDMI via TvService...Pretending to Turn Off")
            self.repaint()
            self.app.processEvents()
            self.screen_on = True
        except:
            pass
    
    def load_image_directory(self, directory):
        logger.info('Displaying Images From: %s', directory)
        if self.time_delay:
            self.files = infinite_directory(directory) 
            self.next_image_with_timer()
        else:
            # show them one at a time, each in a new window
            self.files = iter(list_files(directory))
            self.next_image()
    
    def load_images(self, filenames):
        logger.info('Displaying Images: %s', filenames)
        if self.time_delay:
            self.files = itertools.cycle(filenames)
            self.next_image_with_timer()
        else:
            # show them one at a time, each in a new window
            self.files = iter(filenames)
            self.next_image()
            
    def next_image_with_timer(self):
        if self.use_on_off_schedule and self.should_turn_screen_off():
            logger.info("Screen should be off, re-evaluating in 5 seconds...")
            self.turn_screen_off()
            self.app.processEvents()
            QtCore.QTimer.singleShot(5000, lambda: self.next_image_with_timer())
            return
        else:
            self.turn_screen_on()
            
        start = time.time()
        self.next_image()
        end = time.time()
        delay = self.time_delay - (end - start)
        logger.info("Processing image took %s, sleeping for %s", (end - start), delay)
        if delay <= 0:
            delay = 1
        QtCore.QTimer.singleShot(delay * 1000, lambda: self.next_image_with_timer())
            
    def should_turn_screen_off(self):
        now = time.localtime()
        logger.debug("now=%s", now)
        if now.tm_wday in range(0, 5):
            if now.tm_hour < 5:
                return True
            elif now.tm_hour == 5 and now.tm_min < 30:
                return True
            elif now.tm_hour == 7 and now.tm_min >= 30:
                return True
            elif now.tm_hour > 7 and now.tm_hour < 16:
                return True
            elif now.tm_hour >= 20:
                return True
        else:
            if now.tm_hour < 8:
                return True
            elif now.tm_hour >= 20:
                return True
        return False
    
    def next_image(self):
        if not self.files:
            return False
            
        if self.image_queue.empty():
            fname = self.files.next()
            img = self.prepare_image(fname)
            self.image_queue.put((fname, img))
        
        self.display_image()
        if self.files:
            try:
                fname = self.files.next()
                img = self.prepare_image(fname)
                self.image_queue.put((fname, img))
            except StopIteration:
                self.files = None
            
        return True
        

    def prepare_image(self, filename):
        """Loads an image from a file and performs any needed analysis and modifications
        then returns the prepared image as an RGB OpenCV image.
        """
        
        dirname, raw_filename = path.split(filename)
        raw_filename, _ = path.splitext(raw_filename)
        processed_filename = path.join(dirname, '.processed-' + raw_filename + '.jpg')
        
        if path.exists(processed_filename):
            logger.info("Using prepared image %s", processed_filename)
            
            img = cv2.imread(processed_filename)
            self.raw_img = img
            final_img = img
        else:
            img = cv2.imread(filename)
            self.raw_img = img
            logger.info("Preparing %s: %s %s", filename, img.shape, img.dtype)
        
            final_img = None
            if self.use_face_detection:
                final_img = self.face_centering_crop(img, debug=False)
        
            if final_img is None:
                height, width, _ = img.shape
                if width > height:
                    final_img = self.crop_center(img, (self.width(), self.height()))
                else:
                    final_img = self.background_blur(img)            
            final_img = self.scale_to_fit(final_img, (self.width(), self.height()))
            cv2.imwrite(processed_filename, final_img)
        self.final_img = final_img
    
        return cv2.cvtColor(final_img, cv2.cv.CV_BGR2RGB)
        
    def display_image(self):
        """Displays an RGB OpenCV image."""
        
        fname, img = self.image_queue.get_nowait()
        logger.info("displaying %s...", fname)

        # frame = cv2.flip(frame, 1)
        image = QtGui.QImage(img, img.shape[1], img.shape[0],
                       img.strides[0], QtGui.QImage.Format_RGB888)

        pixmap = QtGui.QPixmap.fromImage(image)
        scaled = pixmap.scaled(self.imageLabel.size(), QtCore.Qt.KeepAspectRatio)
        
        self.imageLabel.setScaledContents(False)
        self.imageLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.imageLabel.setPixmap(scaled)

        # ensure UI gets updated        
        self.app.processEvents()
        
    def crop_center(self, img, target_size):
        image_height, image_width, _ = img.shape
        target_width, target_height = target_size
        
        r = image_width / float(image_height)
        if image_width > image_height: 
            w = target_width
            h = int(w / r)
        else:
            h = target_height
            w = int(r * h)
        img = cv2.resize(img, (w, h), interpolation=cv2.cv.CV_INTER_CUBIC)
        
        target_img = np.zeros((target_height, target_width, 3), dtype='uint8')
        offset_x = (w - target_width) // 2
        offset_y = (h - target_height) // 2
        logger.info("cropping at center: %s x %s -> %s x %s -> %s x %s + (%s, %s)", image_width, image_height, w, h, target_width, target_height, offset_x, offset_y)

        target_img[:] = img[offset_y:offset_y + target_height, offset_x:offset_x + target_width]
        return target_img
        
    def mousePressEvent(self, QMouseEvent):
        if not self.next_image():
            sys.exit(0)
            
        # print mouse position
        x, y = QMouseEvent.x(), QMouseEvent.y()
        pixel = self.final_img[y:y + 1 , x:x + 1 , :]

        rgb = cv2.cvtColor(pixel, cv2.cv.CV_BGR2RGB)[0, 0]
        hsv = cv2.cvtColor(pixel, cv2.COLOR_BGR2HSV)[0, 0]
        ycrcb = cv2.cvtColor(pixel, cv2.COLOR_BGR2YCR_CB)[0, 0]

        logger.info("Pixel %s: rgb=%s, hsv=%s, ycrcb=%s", (x, y), rgb, hsv, ycrcb) 
        
    def scale_to_fit(self, img, max_size):
        image_height, image_width, _ = img.shape
        target_width, target_height = max_size
        
        if image_height == target_height and image_width == target_width:
            return img
        
        r = image_width / float(image_height)
        if image_width > image_height: 
            w = target_width
            h = int(w / r)
        else:
            h = target_height
            w = int(r * h)
              
        img = cv2.resize(img, (w, h), interpolation=cv2.cv.CV_INTER_CUBIC)
        logger.debug("scaling to fit: from=%s, to=%s, max=%s", (image_width, image_height), (w, h), max_size)
        return img

    def background_blur(self, img):
        """Creates a blurred background for displaying portrait images on a landscape
        canvas, this includes the original portrait image on the newly created background.
        """
        
        brush_width = 5
        
        img = self.scale_to_fit(img, (self.width(), self.height()))
        logger.debug("scaled: %s", img.shape)
        
        # we need too add a buffer of brush width because we can't smear all the way to the edge otherwise
        buffer_width = brush_width * 1
        bg = np.ndarray((self.height(), self.width() + 2 * buffer_width, 3), dtype="uint8")
        bg[:] = 255
        logger.debug("bg: %s", bg.shape)
        
        # create the background image by starting with the source image
        # centered on the background
        _, bg_width, _ = bg.shape
        _, img_width, _ = img.shape
        img_left = (bg_width / 2) - (img_width / 2)
        bg[: , img_left:img_left + img_width] = img
        
        # perform a series of smears to cover the background
        smear_iterations = img_left / brush_width
        logger.debug("smear_iterations = %s", smear_iterations)
        for i in xrange(smear_iterations):
            smear_start_x = img_left + img_width - brush_width
            self.smear_right(bg, smear_start_x + brush_width * i, brush_width)
            smear_start_x = img_left + brush_width
            self.smear_left(bg, smear_start_x - brush_width - brush_width * i, brush_width)
            bg = cv2.GaussianBlur(bg, (brush_width, brush_width), 0)
        
        # Copy the source back into the middle
        bg[: , img_left:img_left + img_width] = img
        
        # smear the edges coming off the original so that its not a harsh
        # transition to the background
        edge_smear_brush_width = 5
        edge_smear_strength = 0.5
        self.smear_right(bg, img_left + img_width - 2, edge_smear_brush_width, smudge_length=5, pressure=edge_smear_strength)
        self.smear_left(bg, img_left + 2, edge_smear_brush_width, smudge_length=5, pressure=edge_smear_strength)
        
        # chop off the buffer we added to return the correct sized image
        return bg[:, buffer_width:buffer_width + self.width()]
    
    def blur_region(self, img, region, strength):
        """Blurs a region of the image using a Gaussian blur."""
        
        x, y, w, h = region
        src = img[y:y + h , x:x + w]
        img[y:y + h , x:x + w] = cv2.GaussianBlur(src, (strength, strength), 0)
        
    def smear_right(self, img, smear_start_x, brush_width, smudge_length=None, pressure=0.5):
        """Smears the image to the right."""
        
        if not smudge_length:
            smudge_length = img.shape[1] - smear_start_x - brush_width - 1
        for x in xrange(smear_start_x, smear_start_x + smudge_length):
            # copy the source image to make the brush
            brush = np.copy(img[: , x:x + brush_width])
            
            # paste the brush with an alpha onto the original img
            dst_x = x + 1
            img[: , dst_x:dst_x + brush_width] = cv2.addWeighted(img[: , dst_x:dst_x + brush_width], 1 - pressure, brush, pressure, 0.0)
    
    def smear_left(self, img, smear_start_x, brush_width, smudge_length=None, pressure=0.5):
        """Smears the image to the right."""
        
        if not smudge_length:
            smudge_length = smear_start_x - 1
        for x in xrange(smear_start_x, smear_start_x - smudge_length, -1):
            # copy the source image to make the brush
            brush = np.copy(img[: , x:x + brush_width])
            
            # paste the brush with an alpha onto the original img
            dst_x = x - 1
            img[: , dst_x:dst_x + brush_width] = cv2.addWeighted(img[: , dst_x:dst_x + brush_width], 1 - pressure, brush, pressure, 0.0)
        
    def face_centering_crop(self, img, debug=False):
        """Crops a portrait image down to a landscape portrait that attempts to keep faces."""
                        
        image_height, image_width, _ = img.shape
        original = img
        img = np.copy(img)
        target_size = (self.height(), self.width())
        
        # detect faces
        faces = []
        for cascade in self.face_cascades:
            for angle in [0, -15, 15, -30, 30]:
                rotation_matrix = cv2.getRotationMatrix2D((image_width / 2, image_height / 2), angle, 1)
                rotated_img = cv2.warpAffine(img, rotation_matrix, (image_width, image_height))
                gray = cv2.cvtColor(rotated_img, cv2.COLOR_BGR2GRAY)
                faces.extend(cascade.detectMultiScale(gray,
                    scaleFactor=1.05,
                    minNeighbors=3,
                    minSize=(150, 150),
                    flags=cv2.cv.CV_HAAR_FIND_BIGGEST_OBJECT | cv2.cv.CV_HAAR_DO_ROUGH_SEARCH
                    ));
        
        filtered_faces = []
        for face in faces:
            x, y, w, h = face
            logger.debug("found face: %s", (x, y, w, h))
            if self.skin_found(img, face):
                self.draw_rectangle(img, face, (0, 255, 0), 4)
                filtered_faces.append(face)
            else:
                self.draw_rectangle(img, face, (0, 0, 255), 2)
        if not filtered_faces:
            return None
        
        faces_bound_scaled, all_faces_bounded = self.get_faces_bounds(img, filtered_faces, target_size)
        logger.debug("faces-bounded-scaled: %s", faces_bound_scaled)
        self.draw_rectangle(img, faces_bound_scaled, (255, 0, 0), 2)
        
        # faces_bound = self.get_faces_bounds(img, filtered_faces, target_size, scale_image_to_target=False)
        # self.draw_rectangle(img, faces_bound, (255, 255, 0), 2)
        
        # debug = False
        if not debug:
            x, y, w, h = faces_bound_scaled
            cropped = original[y:y + h, x:x + w]
            logger.debug("original: %s, cropped: %s -> (%s,%s)x(%s,%s)", original.shape, cropped.shape, y, y + h, x, x + w)
            return cropped if all_faces_bounded else None
        else:
            debug_img = img
            # Show side by side
#             img = np.ndarray((self.height(), self.width(), 3), dtype='uint8')
#             x, y, w, h = faces_bound_scaled
#             cropped = original[y:y+h, x:x+w]
#             r = .5
#             cropped = self.scale_to_fit(cropped, (self.width() * r, self.height() * r))
#             debug_img = self.scale_to_fit(debug_img, (self.width() * (1-r), self.height() * (1-r)))
#             img[0:debug_img.shape[0], 0:debug_img.shape[1]] = debug_img
#             img[0:cropped.shape[0], debug_img.shape[1]+1:debug_img.shape[1]+1+cropped.shape[1]] = cropped
            return debug_img
        
    def skin_found(self, img, face):
        """Determines if the face contains skin.
        
        Based on: http://www.pyimagesearch.com/2014/08/18/skin-detection-step-step-example-using-python-opencv/
        """
        
        x, y, w, h = face
        face_img = img[y:y + h, x:x + w]
        
        # define the upper and lower boundaries of the HSV pixel
        # intensities to be considered 'skin'
        # lower = np.array([0, 48, 80], dtype = "uint8")
        # upper = np.array([20, 255, 255], dtype = "uint8")
        # converted = cv2.cvtColor(face_img, cv2.COLOR_BGR2HSV)
        # skin_mask = cv2.inRange(converted, lower, upper)
        # apply a series of erosions and dilations to the mask
        # using an elliptical kernel
        # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
        # skin_mask = cv2.erode(skin_mask, kernel, iterations = 2)
        # skin_mask = cv2.dilate(skin_mask, kernel, iterations = 2)
     
        # blur the mask to help remove noise, then apply the
        # mask to the frame
        # skin_mask = cv2.GaussianBlur(skin_mask, (3, 3), 0)
        
        skin_ycrcb_mint = np.array((0, 133, 77), np.uint8)
        skin_ycrcb_maxt = np.array((255, 173, 127), np.uint8)
        im_ycrcb = cv2.cvtColor(face_img, cv2.COLOR_BGR2YCR_CB)
        skin_mask = cv2.inRange(im_ycrcb, skin_ycrcb_mint, skin_ycrcb_maxt)

        
        
        skin_ratio = np.sum(skin_mask > 0) / float(w * h)
        logger.debug("skin ratio: %s, %s", np.sum(skin_mask > 0), skin_ratio)
        
        # Create a blank 300x300 black image
        # skin_coloring = np.zeros((w, h, 3), np.uint8)
        # Fill image with red color(set each pixel to red)
        # skin_coloring[skin_mask] = (30, 1, 1)
        # skin_coloring = cv2.bitwise_and(skin_coloring,skin_coloring,mask = skin_mask)
        img[y:y + h, x:x + w][skin_mask > 0] = (255, 0, 0)
        
        return skin_ratio > .2
        
        # skin = cv2.bitwise_and(img, img, mask = skinMask)
        

    def draw_point(self, img, center_x, center_y, color, radius):        
        cv2.rectangle(img, (center_x - radius, center_y - radius), (center_x + radius, center_y + radius), color, thickness=cv2.cv.CV_FILLED)
        
    def draw_rectangle(self, img, bounds, color, width):
        x, y, w, h = bounds
        # cv2.rectangle(img, (bounds[0], bounds[1]), (bounds[0] + bounds[2], bounds[1] + bounds[3]), color, width)
        cv2.rectangle(img, (x, y), (x + w, y + h), color, width)
    
    def get_faces_bounds(self, img, faces, target_size, scale_image_to_target=True):
        """Returns the bounding rectangle to use that ensures all faces are visible
        
        scale_image_to_target = True  : Scales the image's maximum dimension to fit the targets maximum dimension, this will down-sample the image
                                False : Crops the image to the target size keeping the original resolution 1:1
        """
        
        logger.debug("Finding bounds for faces: %s", len(faces))
        logger.debug("image size: %s", img.shape[0:2])
        logger.debug("target size: %s", target_size)
        image_height, image_width = img.shape[0:2]
        target_height, target_width = target_size
        
        # computer the center of all the faces and the bounding 
        # rectangle that will capture them all
        center_x, center_y = 0, 0
        min_x, min_y = sys.maxint, sys.maxint
        max_x, max_y = -sys.maxint, -sys.maxint
        for (x, y , w, h) in faces:
            face_center_x = x + w * .5
            face_center_y = y + h * .5
            center_x += face_center_x
            center_y += face_center_y
            min_x = min(min_x, x)
            min_y = min(min_y, y)
            max_x = max(max_x, x)
            max_y = max(max_y, y)
        center_x = int(center_x / len(faces))
        center_y = int(center_y / len(faces))
        
        logger.debug("Raw-Face-Bounds: %s", (min_x, min_y, max_x, min_y))
        logger.debug("Raw-Face-Center: %s", (center_x, center_y))
        self.draw_point(img, center_x, center_y, (0, 0, 255), 10)
        
        # computer the weighting of the center of the faces relative to 
        # the images actual center
        left_weight = center_x / float(image_width)
        top_weight = center_y / float(image_height)
        logger.debug("weights: %s x %s", left_weight, top_weight)
        
        if scale_image_to_target:      
            target_ratio = target_height / float(target_width)
            if target_ratio * image_width < image_height / target_ratio:
                bound_width = image_width
                bound_height = int(target_ratio * image_width)
            else:
                target_ratio = target_width / float(target_height)
                bound_width = int(image_height / target_ratio)
                bound_height = image_height
            
            bound_x = int(max(0, center_x - (bound_width * .5)))        
            bound_y = int(max(0, center_y - (bound_height * .5)))            
        else:
            bound_x = int(max(0, center_x - (target_width * .5)))        
            bound_width = target_width
                
            bound_y = int(max(0, center_y - (target_height * .5)))
            bound_height = target_height
        
        if bound_x + bound_width > image_width:
            overage = (bound_x + bound_width) - image_width
            bound_x -= overage
        if bound_x < 0:
            bound_x = 0        
        
        if bound_y + bound_height > image_height:                
            overage = (bound_y + bound_height) - image_height 
            bound_y -= overage
        if bound_y < 0:
            bound_y = 0

        logger.debug("Face-Bounds: %s", (bound_x, bound_y, bound_width, bound_height))
        all_faces_bounded = bound_x <= min_x and bound_x + bound_width >= max_x and bound_y <= min_y and bound_y + bound_height >= max_y
        logger.debug("All-Faces: %s", all_faces_bounded)
        return (bound_x, bound_y, bound_width, bound_height), all_faces_bounded

class infinite_directory(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.files = []
        self.index = 0
    
    def __iter__(self):
        return self
    
    def next(self):
        if self.index >= len(self.files):            
            self.files = list_files(self.dirname)
            shuffle(self.files)
            logger.info("Scanning directory %s for pictures, found: %s", self.dirname, self.files)
            self.index = 0
        
        filename = self.files[self.index]
        self.index += 1
        return filename
    
def list_files(dirname):
    from os import listdir
    from os.path import isfile, join
    return [ join(dirname, f) for f in listdir(dirname) if isfile(join(dirname, f)) and (f.lower().endswith('png') or f.lower().endswith('jpg'))]

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Smart slideshow.')
    parser.add_argument('photos', metavar='photos-dir',
                        help='directory of photos to show')
    parser.add_argument('--fullscreen', dest='fullscreen', action='store_const',
                        const=True, default=False,
                        help='display slide show in fullscreen')
    parser.add_argument('--delay', dest='time_delay', action='store',
                       type=int, default=None,
                        help='time to wait between images, default is advance on click')
    parser.add_argument('--use-on-off-schedule', dest='use_on_off_schedule', action='store_const',
                        const=True, default=False,
                        help='automatically turn the display on and off')
    parser.add_argument('--use-face-detection', dest='use_face_detection', action='store_const',
                        const=True, default=False,
                        help='use face detection when cropping')

    args = parser.parse_args()
    
    signal.signal(signal.SIGINT, lambda *args: QtGui.QApplication.quit())
    app = QtGui.QApplication(sys.argv)
    logger.debug("Starting image viewer")    
    imageViewer = ImageViewer(app, **vars(args))
    imageViewer.load_image_directory(args.photos)
    sys.exit(app.exec_())
